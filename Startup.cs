﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using VueCliMiddleware;

namespace vue_cli_3_and_dot_net_core_2._2_template
{
    public class Startup
    {
        public IConfiguration Configuration { get; private set; }

        public Startup(IConfiguration configuration)
        {
          Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
          // Add MVC
          services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

          // In production the built vue files will be in the dist folder.
          services.AddSpaStaticFiles(configuration => {
            configuration.RootPath = "clientapp/dist";
          });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            } else {
              app.UseExceptionHandler("/Error");
              // The default HSTS value is 30 days. You may want to change this for Production scenarios, see https://aka.ms/aspnetcore-hsts.
              app.UseHsts();
              app.UseHttpsRedirection();
            }

            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseMvc(routes => 
            {
              routes.MapRoute(
                name: "default",
                template: "{controller}/{action=Index}/{id?}"
              );
            });

            app.UseSpa(spa =>
            {
              spa.Options.SourcePath = "clientapp";

              if(env.IsDevelopment())
              {
                spa.UseVueCli(npmScript: "serve", port: 8081);
              }
            });

        }
    }
}
